## Requirements:

### Required:

- Docker 24.0.4

### Recommended:

- Make

## Commands

- Build the project with

  ```
  make build
  # or
  docker compose build
  ```

- Start the project with

  ```
  make start
  # or
  docker compose up
  ```

  Project starts in http://127.0.0.1:8000/

- Make db migrations with

  ```
  make migrate
  # or
  docker compose run --rm app python manage.py makemigrations
  docker compose run --rm app python manage.py migrate
  ```

- Stop containers with
  ```
  make down
  # or
  docker compose down
  ```

## Using the api

You can go to http://127.0.0.1:8000/graphql to make the available queries through the graphql console
