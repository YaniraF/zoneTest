build:
	docker compose build

start:
	docker compose up

down:
	docker compose down

migrate:
	docker compose run --rm app python manage.py makemigrations
	docker compose run --rm app python manage.py migrate

migrate-local:
	python manage.py makemigrations
	python manage.py migrate
