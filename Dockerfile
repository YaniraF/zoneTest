FROM python:3.10.2-slim-bullseye

RUN apt-get update && apt-get install -y gcc libpcre3-dev

WORKDIR /app

ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt .

RUN pip install -r requirements.txt 

COPY . .

CMD python /app/manage.py runserver 0.0.0.0:8000