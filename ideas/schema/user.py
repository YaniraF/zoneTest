import strawberry
from ideas.models.userModel import User
from ideas.services.userService import UserService
from ideas.types.userType import UserType
from typing import List


@strawberry.type
class UserQuery:
    @strawberry.field
    def users(self, id: int = None) -> List[UserType]:
        return UserService.get(id)


@strawberry.type
class UserMutation:
    @strawberry.field
    def create_user(self, name: str) -> UserType:
        return UserService.create(name)

    @strawberry.field
    def update_user(self, id: int, name: str) -> UserType:
        return UserService.update(id, name)

    @strawberry.field
    def delete_user(self, id: int) -> bool:
        return UserService.delete(id)
