import strawberry
from ideas.models.ideaModel import Idea, Visibility
from ideas.services.ideaService import IdeaService
from ideas.types.ideaType import IdeaType
from typing import List


@strawberry.type
class IdeaQuery:
    @strawberry.field
    def ideas(self, author_id: int = None, visibility: str = Visibility.PUBLIC) -> List[IdeaType]:
        return IdeaService.get(author_id, visibility)


@strawberry.type
class IdeaMutation:
    @strawberry.field
    def create_idea(self, title: str, author_id: int, description: str, visibility: str) -> IdeaType:
        return IdeaService.create(title, author_id, description, visibility)

    @strawberry.field
    def update_idea(self, id: int, title: str, description: str, visibility: str) -> IdeaType:
        return IdeaService.update(id, title, description, visibility)

    @strawberry.field
    def delete_idea(self, id: int) -> bool:
        return IdeaService.delete()
