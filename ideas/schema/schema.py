import strawberry
from ideas.models.followModel import Follow
from ideas.schema.follow import FollowMutation, FollowQuery
from ideas.schema.idea import IdeaMutation, IdeaQuery
from ideas.schema.user import UserMutation, UserQuery


@strawberry.type
class Query(IdeaQuery, FollowQuery, UserQuery):
    pass


@strawberry.type
class Mutation(IdeaMutation, FollowMutation, UserMutation):
    pass


schema = strawberry.Schema(query=Query, mutation=Mutation)
