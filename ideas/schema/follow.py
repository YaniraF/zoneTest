
import strawberry
from ideas.models.followModel import Follow
from ideas.services.followService import FollowService
from ideas.types.followType import FollowType
from typing import List


@strawberry.type
class FollowQuery():
    @strawberry.field
    def follows(self, author_id: int = None, follower_id: int = None) -> List[FollowType]:
        return FollowService.get(author_id, follower_id)


@strawberry.type
class FollowMutation():
    @strawberry.field
    def create_follow(self, author_id: int, follower_id: int) -> FollowType:
        return FollowService.create(author_id, follower_id)

    @strawberry.field
    def update_follow(self, id: int, status: str) -> FollowType:
        return FollowService.update(id, status)

    @strawberry.field
    def delete_follow(self, id: int) -> bool:
        return FollowService.delete(id)
