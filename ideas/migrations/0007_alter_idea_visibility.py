# Generated by Django 4.2.3 on 2023-07-31 21:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ideas', '0006_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='idea',
            name='visibility',
            field=models.CharField(auto_created='public', choices=[('public', 'Public'), ('protected', 'Protected'), ('private', 'Private')], default='public', max_length=10),
        ),
    ]
