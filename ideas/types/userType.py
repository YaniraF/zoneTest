import strawberry
from ideas.models.userModel import User


@strawberry.django.type(User)
class UserType:
    id: int
    name: str
