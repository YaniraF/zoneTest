from enum import Enum
import strawberry


@strawberry.enum
class FollowStatus(Enum):
    PENDING = "pending"
    ACCEPTED = "accepted"
    DECLINED = "declined"


@strawberry.type
class FollowType:
    id: int
    author_id: int
    follower_id: int
    timestamp: str
    status: FollowStatus
