from enum import Enum
import strawberry
from ..models.ideaModel import Idea


@strawberry.django.type(Idea)
class IdeaType:
    id: int
    title: str
    author_id: int
    description: str
    visibility: str
    timestamp: str
