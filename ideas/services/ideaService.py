import strawberry
from ideas.models.ideaModel import Idea, Visibility
from ideas.types.ideaType import IdeaType
from typing import List


class IdeaService:
    def get(author_id: int = None, visibility: str = Visibility.PUBLIC) -> List[IdeaType]:
        ideas = Idea.objects.all()
        if author_id:
            ideas = ideas.filter(author_id=author_id)

        return ideas

    def create(title: str, author_id: int, description: str, visibility: str) -> IdeaType:
        idea = Idea(title=title, description=description,
                    visibility=visibility, author_id=author_id)
        idea.save()
        return idea

    def update(id: int, title: str, description: str, visibility: str) -> IdeaType:
        idea = Idea.objects.get(id=id)
        idea.title = title
        idea.description = description
        idea.visibility = visibility
        idea.save()

        return idea

    def delete(id: int) -> bool:
        idea = Idea.objects.get(pk=id)
        idea.delete()

        return True
