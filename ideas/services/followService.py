import strawberry
from ideas.models.followModel import Follow
from ideas.types.followType import FollowType
from typing import List


class FollowService():
    def get(author_id: int = None, follower_id: int = None) -> List[FollowType]:
        follows = Follow.objects.all()
        if author_id:
            follows = follows.filter(author_id=author_id)
        if follower_id:
            follows = follows.filter(follower_id=follower_id)

        return follows

    def create(author_id: int, follower_id: int) -> FollowType:
        follow = Follow(author_id=author_id, follower_id=follower_id)
        follow.save()

        return follow

    def update(id: int, status: str) -> FollowType:
        follow = Follow.objects.get(id=id)
        follow.status = status
        follow.save()

        return follow

    def delete(id: int) -> bool:
        follow = Follow.objects.get(pk=id)
        follow.delete()

        return True
