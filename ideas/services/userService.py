import strawberry
from ideas.models.userModel import User
from ideas.types.userType import UserType
from typing import List


class UserService:
    def get(id: int = None) -> List[UserType]:
        users = User.objects.all()
        if id:
            users = users.filter(id=id)

        return users

    def create(name: str) -> UserType:
        user = User(name=name)
        user.save()
        return user

    def update(id: int, name: str) -> UserType:
        user = User.objects.get(id=id)
        user.name = name
        user.save()

        return user

    def delete(id: int) -> bool:
        user = User.objects.get(id=id)
        user.delete()

        return True
