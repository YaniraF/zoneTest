from django.db import models


class Visibility(models.TextChoices):
    PUBLIC = 'public'
    PROTECTED = 'protected'
    PRIVATE = 'private'


class Idea(models.Model):

    title = models.CharField(max_length=60)
    author_id = models.BigIntegerField()
    description = models.TextField(max_length=600)
    visibility = models.CharField(
        max_length=10, choices=Visibility.choices, default=Visibility.PUBLIC, auto_created=Visibility.PUBLIC)
    timestamp = models.TimeField(auto_now_add=True)
