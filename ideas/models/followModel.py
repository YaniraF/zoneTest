from django.db import models
from ideas.models.ideaModel import Idea


class Follow(models.Model):
    class Status(models.TextChoices):
        PENDING = 'pending'
        ACCEPTED = 'accepted'
        DECLINED = 'declined'

    author_id = models.BigIntegerField()
    follower_id = models.BigIntegerField()
    timestamp = models.TimeField(auto_now_add=True)
    status = models.CharField(
        max_length=20, choices=Status.choices, default=Status.PENDING, auto_created=Status.PENDING)
