from django.contrib import admin
from .models.ideaModel import Idea

# Register your models here.
admin.site.register(Idea)
